import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent {
  public toggle: boolean = false;
  @Output() private toggleStatus: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  public togglePanel(): void {
    this.toggle = !this.toggle;
    this.toggleStatus.emit(this.toggle);
  }
}
