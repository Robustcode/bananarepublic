import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { FormModule } from 'src/app/components/form/form.module';
import { ProfileModule } from 'src/app/components/profile/profile.module';
import { SlideModule } from 'src/app/components/slide/slide.module';



@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    FormModule,
    ProfileModule,
    SlideModule
  ]
})
export class LoginModule { }
