import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public toggle: boolean = false;
  private mobile = window.matchMedia("(max-width: 991px)");
  @ViewChild("form") myForm: ElementRef;

  constructor() { }

  public togglePanel(event: boolean): void {
    this.toggle = event;
    if (this.mobile.matches) {
      this.myForm.nativeElement.scrollIntoView({ behavior: "smooth", block: "start" });      
    }
  }

}
